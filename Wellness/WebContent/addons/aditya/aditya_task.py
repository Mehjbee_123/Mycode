# -*- coding: utf-8 -*-
from openerp import models, fields, api
class TodoTask(models.Model):
    _inherit = 'anupam.task'
    name = fields.Char(help="What should I do here?")
    user_id = fields.Many2one('res.users', 'Responsible')
    date_deadline = fields.Date('Deadline')

    @api.multi
    def do_clear_done(self):
      domain = [('is_done', '=', True),
        '|', ('user_id', '=', self.env.uid),
        ('user_id', '=', False)]
      done_recs = self.search(domain)
      done_recs.write({'active': False})
      return True
