{
    'name': 'anupam app',
    'description': 'Manage your personal Tasks with this module.',
    'author': 'anupam jeevan',
    'depends': ['mail'],
    'application': True,
    'data': ['anupam_view.xml'],
}
