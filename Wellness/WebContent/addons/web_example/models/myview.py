from openerp.osv import osv, fields
from openerp.tools.translate import _

class res_partner(osv.osv):
    _inherit = 'res.partner'

    _columns = {
        'username': fields.char('UserName', size=40),
        'password': fields.char('Password', size=40),
        'payerid': fields.char('PayerId', size=40),
        'merchantid': fields.char('MerchantId', size=40),
        }
res_partner()



class res_users(osv.osv):
    _inherit = 'res.users'

    _columns = {
        'refid_pp': fields.char('ReferenceID', size=40),
        }
res_users()

class hr_employee(osv.osv):
    _inherit = 'hr.employee'
    
    _columns = {
        'username': fields.char('UserName', size=40),
        'password': fields.char('Password', size=40),
        'payerid': fields.char('PayerId', size=40),
        }
hr_employee()

