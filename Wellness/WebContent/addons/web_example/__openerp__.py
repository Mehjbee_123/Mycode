{
    'name': "web_example",
    'description': "Basic example of a web  module",
    'category': 'Hidden',
    'depends': ['web','base','hr'],
    'active': True,
    'installable': True,
#'test': ['static/test/readme.html'], 
'data': [
    'views/example_webpage.xml',
    'views/myview.xml',
],
}
