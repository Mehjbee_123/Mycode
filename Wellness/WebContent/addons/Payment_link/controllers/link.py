# -*- coding: utf-8 -*-
import base64
import hashlib
import StringIO
import binascii
from Crypto import Random
from Crypto.Cipher import AES
from openerp import http
from openerp.addons import web
from openerp.addons.account import models
from openerp.addons.account.models import account_invoice
#from openerp.addons.account.models.account_invoice import decrypt
from openerp import models, fields, api
import logging
import urlparse
from urlparse import urlparse, parse_qs
from openerp.cli import Command

import psycopg2

_logger = logging.getLogger(__name__)
 
class Paymentlink(http.Controller):
    """@http.route('/example/detail', type='http', auth='public', website=True)
    def navigate_to_detail_page(self):
        _logger.info(' This is happening inside the a:::::::::::::::::::::::navigate_to_detail_page:::::::::::::::::::y')
        # This will get all company details (in case of multicompany this are multiple records)
        companies = http.request.env['account.invoice'].sudo().search([])
        return http.request.render('web_example.detail_page', {
          # pass company details to the webpage in a variable
          'companies': companies})
    @http.route('/example/update', type='http', auth='public', website=True)
    #@api.model
    def action_invoice_update(self):
        _logger.info(' action_invoice_update:::::::::::::::: ')
        return http.request.render('web_example.update', {})
          # pass company details to the webpage in a variable"""
    

    @http.route('/payment', type='http', auth='public', website=True)
    def render_example_page(self,**kwargs):
	ClientCode='CSPT1' 
        AuthKey='vYvMM2VNtmF53weN' 
        AuthIV='exTZtkW6HL9yPM4Q' 
        ClientUName='pooja.kushwaha_32' 
        ClictPass='CSPT1_SP32' 
        AuthFlag='Y'
        urlVAl= kwargs['query']
        encClass=AESCipher('AuthKey')
	spURL = urlVAl.replace("%2B", "+")
	sp=encClass.decrypt(AuthKey,AuthIV,spURL)
        _logger.info(' decrypted url is ::::::::::::::::::::::: %s' %sp)
        url = "http://localhost:8069/example?query=%s"%sp
        uri = urlparse(url)
        qs = uri.query
        name = ''.join(parse_qs(qs).get('firstName',None))
        status=''.join(parse_qs(qs).get('spRespStatus',None))
        clientTxnId=''.join(parse_qs(qs).get('clientTxnId',None))
	amount=''.join(parse_qs(qs).get('amount',None))
	mobileNo=''.join(parse_qs(qs).get('mobileNo',None))
	clientTxnId=clientTxnId[0:13]
        _logger.info(' clientTxnId is ::::::::::::::::::::::: %s' %clientTxnId)
	if status == 'success':
           status ='paid'
	else:
	   status = 'open'        
    	conn=psycopg2.connect(host="127.0.0.1", port="5432",
        database="Aditya", user="odoo", password="Aviral123")
        src_cr = conn.cursor()
        try:
        # Query to retrieve source model data
          src_cr.execute("""UPDATE account_invoice SET state=%s WHERE number=%s""", (status, clientTxnId))
          conn.commit()
	except psycopg2.Error as e:
	  _logger.info(e.pgerror)
        _logger.info('firstName::::: %s' %name)
	_logger.info(' clientTxnId::::::::::::::::::::::: %s' %clientTxnId)
	_logger.info(' amount::::::::::::::::::::::: %s' %amount)
	_logger.info(' email::::::::::::::::::::::: %s' %''.join(parse_qs(qs).get('email',None)))	
        _logger.info(' mobileNo::::::::::::::::::::::: %s' %mobileNo)
	_logger.info(' RespStatus::::::::::::::::::::::: %s' %status)
        return http.request.render('web_example.example_page', {})

class AESCipher(object):

#ClientCode,AuthKey,AuthIV,ClientUName,ClictPass
    def __init__(self, key): 
        self.bs = 32
        self.key = hashlib.sha256(key.encode()).digest()
 
    def encrypt(self,AuthKey,AuthIV,raw):
        #_logger.info('In side the Ecnription Class tEXT is: %s'% raw)
      	
	shared_key = AuthKey #some random key for a working example
	IV = AuthIV
	clear_text = raw
	aes = AES.new(shared_key, AES.MODE_CBC, IV)
	aes.block_size = 128
	cipher_text = base64.b64encode(aes.encrypt(PKCS7Encoder().encode(clear_text)))
#print(cipher_text)
        return cipher_text

    def decrypt(self,AuthKey,AuthIV,raw):
	shared_key = AuthKey #some random key for a working example
	IV = AuthIV
	cipher_text = raw
   	aes_decrypter = AES.new(shared_key, AES.MODE_CBC, IV)
	aes_decrypter.block_size = 128
        #_logger.info('In side the decrption Class tEXT is:::::::::::::::::::::::::::: %s'% cipher_text)
	clear_text = PKCS7Encoder().decode(aes_decrypter.decrypt(base64.b64decode(cipher_text)))
        #_logger.info('In side the Ecnription Class tEXT is: %s'% clear_text)
        #name = clear_text.firstName
        
	print(clear_text)
	return clear_text

class PKCS7Encoder(object):
    def __init__(self, k=16):
       self.k = k

    ## @param text The padded text for which the padding is to be removed.
    # @exception ValueError Raised when the input padding is missing or corrupt.
    def decode(self, text):
        '''
        Remove the PKCS#7 padding from a text string
        '''
        nl = len(text)
        val = int(binascii.hexlify(text[-1]), 16)
        if val > self.k:
            raise ValueError('Input is not padded or padding is corrupt')

        l = nl - val
        return text[:l]

    ## @param text The text to encode.
    def encode(self, text):
        '''
        Pad an input string according to PKCS#7
        '''
        l = len(text)
        output = StringIO.StringIO()
        val = self.k - (l % self.k)
        for _ in xrange(val):
            output.write('%02x' % val)
        return text + binascii.unhexlify(output.getvalue())
